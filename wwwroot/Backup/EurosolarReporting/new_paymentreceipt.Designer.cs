namespace EurosolarReporting
{
    partial class new_paymentreceipt
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(new_paymentreceipt));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.txtdate = new Telerik.Reporting.DetailSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.table8 = new Telerik.Reporting.Table();
            this.textBox126 = new Telerik.Reporting.TextBox();
            this.lblInvoiceSent = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.lblInvoiceNumber = new Telerik.Reporting.TextBox();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox85 = new Telerik.Reporting.TextBox();
            this.textBox89 = new Telerik.Reporting.TextBox();
            this.textBox90 = new Telerik.Reporting.TextBox();
            this.textBox92 = new Telerik.Reporting.TextBox();
            this.textBox93 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.htmlTextBox1 = new Telerik.Reporting.HtmlTextBox();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.lblBalanceOwing = new Telerik.Reporting.TextBox();
            this.table11 = new Telerik.Reporting.Table();
            this.txtfullretail = new Telerik.Reporting.TextBox();
            this.txtdiscount2 = new Telerik.Reporting.TextBox();
            this.txtstc = new Telerik.Reporting.TextBox();
            this.txtgst = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.pictureBox2 = new Telerik.Reporting.PictureBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.3420963287353516D), Telerik.Reporting.Drawing.Unit.Cm(1.529017448425293D));
            this.textBox7.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox7.Style.BorderColor.Default = System.Drawing.Color.Gray;
            this.textBox7.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox7.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox7.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox7.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox7.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox7.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Name = "Calibri";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(20D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.Value = "Payment Date";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3617086410522461D), Telerik.Reporting.Drawing.Unit.Cm(1.529017448425293D));
            this.textBox9.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox9.Style.BorderColor.Default = System.Drawing.Color.Gray;
            this.textBox9.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox9.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox9.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox9.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox9.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox9.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Name = "Calibri";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(20D);
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.Value = "Payment Amount $";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1047978401184082D), Telerik.Reporting.Drawing.Unit.Cm(1.529017448425293D));
            this.textBox11.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox11.Style.BorderColor.Default = System.Drawing.Color.Gray;
            this.textBox11.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox11.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox11.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox11.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox11.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox11.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox11.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox11.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox11.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox11.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox11.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Name = "Calibri";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(20D);
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox11.StyleName = "";
            this.textBox11.Value = "CC Surcharge $";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.17077112197876D), Telerik.Reporting.Drawing.Unit.Cm(1.529017448425293D));
            this.textBox12.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox12.Style.BorderColor.Default = System.Drawing.Color.Gray;
            this.textBox12.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox12.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox12.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox12.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox12.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox12.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Name = "Calibri";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(20D);
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox12.Value = "Payment Method";
            // 
            // txtdate
            // 
            this.txtdate.Height = Telerik.Reporting.Drawing.Unit.Cm(23.799999237060547D);
            this.txtdate.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.table8,
            this.table3,
            this.table4,
            this.table1,
            this.table11});
            this.txtdate.Name = "txtdate";
            this.txtdate.Style.Font.Name = "Calibri";
            this.txtdate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9378803194267675E-05D));
            this.pictureBox1.MimeType = "image/png";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(23.599899291992188D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Style.Font.Bold = true;
            this.pictureBox1.Style.Font.Name = "Calibri";
            this.pictureBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(20D);
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // table8
            // 
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.1898369789123535D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.6837050914764404D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.88000082969665527D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.88000082969665527D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.8800010085105896D)));
            this.table8.Body.SetCellContent(2, 0, this.textBox126);
            this.table8.Body.SetCellContent(1, 1, this.lblInvoiceSent);
            this.table8.Body.SetCellContent(2, 1, this.textBox20);
            this.table8.Body.SetCellContent(1, 0, this.textBox6);
            this.table8.Body.SetCellContent(0, 0, this.textBox15);
            this.table8.Body.SetCellContent(0, 1, this.lblInvoiceNumber);
            tableGroup1.Name = "tableGroup13";
            tableGroup2.Name = "tableGroup14";
            this.table8.ColumnGroups.Add(tableGroup1);
            this.table8.ColumnGroups.Add(tableGroup2);
            this.table8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox15,
            this.lblInvoiceNumber,
            this.textBox6,
            this.lblInvoiceSent,
            this.textBox126,
            this.textBox20});
            this.table8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.30000004172325134D), Telerik.Reporting.Drawing.Unit.Cm(12.59999942779541D));
            this.table8.Name = "table8";
            tableGroup4.Name = "group";
            tableGroup5.Name = "group28";
            tableGroup6.Name = "group29";
            tableGroup3.ChildGroups.Add(tableGroup4);
            tableGroup3.ChildGroups.Add(tableGroup5);
            tableGroup3.ChildGroups.Add(tableGroup6);
            tableGroup3.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup3.Name = "detailTableGroup7";
            this.table8.RowGroups.Add(tableGroup3);
            this.table8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.8735418319702148D), Telerik.Reporting.Drawing.Unit.Cm(2.6400027275085449D));
            this.table8.Style.Color = System.Drawing.Color.White;
            this.table8.Style.Font.Name = "Calibri";
            this.table8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            // 
            // textBox126
            // 
            this.textBox126.Name = "textBox126";
            this.textBox126.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1898369789123535D), Telerik.Reporting.Drawing.Unit.Cm(0.8800010085105896D));
            this.textBox126.Style.Color = System.Drawing.Color.White;
            this.textBox126.Style.Font.Bold = true;
            this.textBox126.Style.Font.Name = "Calibri";
            this.textBox126.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(20D);
            this.textBox126.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(30D);
            this.textBox126.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox126.StyleName = "";
            this.textBox126.Value = "";
            // 
            // lblInvoiceSent
            // 
            this.lblInvoiceSent.Name = "lblInvoiceSent";
            this.lblInvoiceSent.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6837050914764404D), Telerik.Reporting.Drawing.Unit.Cm(0.88000082969665527D));
            this.lblInvoiceSent.Style.Font.Bold = false;
            this.lblInvoiceSent.Style.Font.Name = "Calibri";
            this.lblInvoiceSent.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.lblInvoiceSent.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.lblInvoiceSent.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblInvoiceSent.Value = "";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6837050914764404D), Telerik.Reporting.Drawing.Unit.Cm(0.8800010085105896D));
            this.textBox20.Style.Font.Bold = false;
            this.textBox20.Style.Font.Name = "Calibri";
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox20.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1898369789123535D), Telerik.Reporting.Drawing.Unit.Cm(0.88000082969665527D));
            this.textBox6.Style.Color = System.Drawing.Color.White;
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Name = "Calibri";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(20D);
            this.textBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(30D);
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox6.StyleName = "";
            this.textBox6.Value = "";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1898369789123535D), Telerik.Reporting.Drawing.Unit.Cm(0.88000082969665527D));
            this.textBox15.Style.Color = System.Drawing.Color.White;
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.Font.Name = "Calibri";
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(20D);
            this.textBox15.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(30D);
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox15.StyleName = "";
            this.textBox15.Value = "";
            // 
            // lblInvoiceNumber
            // 
            this.lblInvoiceNumber.Name = "lblInvoiceNumber";
            this.lblInvoiceNumber.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6837050914764404D), Telerik.Reporting.Drawing.Unit.Cm(0.88000082969665527D));
            this.lblInvoiceNumber.Style.Font.Bold = false;
            this.lblInvoiceNumber.Style.Font.Name = "Calibri";
            this.lblInvoiceNumber.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.lblInvoiceNumber.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.lblInvoiceNumber.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblInvoiceNumber.StyleName = "";
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.26454496383667D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(11.136100769042969D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.699999988079071D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.699999988079071D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.699999988079071D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D)));
            this.table3.Body.SetCellContent(0, 0, this.textBox84);
            this.table3.Body.SetCellContent(3, 0, this.textBox83);
            this.table3.Body.SetCellContent(3, 1, this.textBox85);
            this.table3.Body.SetCellContent(2, 0, this.textBox89);
            this.table3.Body.SetCellContent(2, 1, this.textBox90);
            this.table3.Body.SetCellContent(1, 0, this.textBox92);
            this.table3.Body.SetCellContent(1, 1, this.textBox93);
            this.table3.Body.SetCellContent(0, 1, this.textBox28);
            tableGroup7.Name = "tableGroup4";
            tableGroup8.Name = "tableGroup5";
            this.table3.ColumnGroups.Add(tableGroup7);
            this.table3.ColumnGroups.Add(tableGroup8);
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox84,
            this.textBox28,
            this.textBox92,
            this.textBox93,
            this.textBox89,
            this.textBox90,
            this.textBox83,
            this.textBox85});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.099999949336051941D), Telerik.Reporting.Drawing.Unit.Cm(3.9000000953674316D));
            this.table3.Name = "table3";
            tableGroup10.Name = "group9";
            tableGroup11.Name = "group12";
            tableGroup12.Name = "group11";
            tableGroup13.Name = "group10";
            tableGroup9.ChildGroups.Add(tableGroup10);
            tableGroup9.ChildGroups.Add(tableGroup11);
            tableGroup9.ChildGroups.Add(tableGroup12);
            tableGroup9.ChildGroups.Add(tableGroup13);
            tableGroup9.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup9.Name = "detailTableGroup2";
            this.table3.RowGroups.Add(tableGroup9);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.400646209716797D), Telerik.Reporting.Drawing.Unit.Cm(2.7999999523162842D));
            this.table3.Style.Font.Name = "Calibri";
            this.table3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            // 
            // textBox84
            // 
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.26454496383667D), Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D));
            this.textBox84.Style.Color = System.Drawing.Color.White;
            this.textBox84.Style.Font.Bold = true;
            this.textBox84.Style.Font.Name = "Calibri";
            this.textBox84.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(20D);
            this.textBox84.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(30D);
            this.textBox84.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox84.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox84.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            // 
            // textBox83
            // 
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.26454496383667D), Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D));
            this.textBox83.Style.Color = System.Drawing.Color.White;
            this.textBox83.Style.Font.Bold = true;
            this.textBox83.Style.Font.Name = "Calibri";
            this.textBox83.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(20D);
            this.textBox83.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(30D);
            this.textBox83.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox83.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox83.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox83.StyleName = "";
            // 
            // textBox85
            // 
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.136102676391602D), Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D));
            this.textBox85.Style.Color = System.Drawing.Color.White;
            this.textBox85.Style.Font.Bold = false;
            this.textBox85.Style.Font.Name = "Calibri";
            this.textBox85.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox85.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox85.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox85.StyleName = "";
            this.textBox85.Value = "=Fields.ContEmail";
            // 
            // textBox89
            // 
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.26454496383667D), Telerik.Reporting.Drawing.Unit.Cm(0.699999988079071D));
            this.textBox89.Style.Color = System.Drawing.Color.White;
            this.textBox89.Style.Font.Bold = true;
            this.textBox89.Style.Font.Name = "Calibri";
            this.textBox89.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(20D);
            this.textBox89.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(30D);
            this.textBox89.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox89.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox89.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox89.StyleName = "";
            // 
            // textBox90
            // 
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.136102676391602D), Telerik.Reporting.Drawing.Unit.Cm(0.699999988079071D));
            this.textBox90.Style.Color = System.Drawing.Color.White;
            this.textBox90.Style.Font.Bold = false;
            this.textBox90.Style.Font.Name = "Calibri";
            this.textBox90.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox90.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox90.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox90.StyleName = "";
            this.textBox90.Value = "=Fields.CustPhone";
            // 
            // textBox92
            // 
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2645447254180908D), Telerik.Reporting.Drawing.Unit.Cm(0.69999992847442627D));
            this.textBox92.Style.Color = System.Drawing.Color.White;
            this.textBox92.Style.Font.Bold = true;
            this.textBox92.Style.Font.Name = "Calibri";
            this.textBox92.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(20D);
            this.textBox92.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(30D);
            this.textBox92.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox92.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox92.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox92.StyleName = "";
            // 
            // textBox93
            // 
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.136100769042969D), Telerik.Reporting.Drawing.Unit.Cm(0.69999992847442627D));
            this.textBox93.Style.Color = System.Drawing.Color.White;
            this.textBox93.Style.Font.Bold = false;
            this.textBox93.Style.Font.Name = "Calibri";
            this.textBox93.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox93.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox93.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox93.StyleName = "";
            this.textBox93.Value = "=Fields.ContMobile";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.136101722717285D), Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D));
            this.textBox28.Style.Color = System.Drawing.Color.White;
            this.textBox28.Style.Font.Bold = false;
            this.textBox28.Style.Font.Name = "Calibri";
            this.textBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox28.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox28.StyleName = "";
            this.textBox28.Value = "=Fields.ContFirst +\" \" + Fields.ContLast";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(15.05291748046875D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(2D)));
            this.table4.Body.SetCellContent(0, 0, this.htmlTextBox1);
            tableGroup14.Name = "tableGroup5";
            this.table4.ColumnGroups.Add(tableGroup14);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.htmlTextBox1});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.30000004172325134D), Telerik.Reporting.Drawing.Unit.Cm(8.59999942779541D));
            this.table4.Name = "table4";
            tableGroup16.Name = "group9";
            tableGroup15.ChildGroups.Add(tableGroup16);
            tableGroup15.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup15.Name = "detailTableGroup2";
            this.table4.RowGroups.Add(tableGroup15);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.05291748046875D), Telerik.Reporting.Drawing.Unit.Cm(2D));
            this.table4.Style.Color = System.Drawing.Color.White;
            this.table4.Style.Font.Name = "Calibri";
            this.table4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            // 
            // htmlTextBox1
            // 
            this.htmlTextBox1.Name = "htmlTextBox1";
            this.htmlTextBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.05291748046875D), Telerik.Reporting.Drawing.Unit.Cm(2D));
            this.htmlTextBox1.Style.Font.Name = "Calibri";
            this.htmlTextBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.htmlTextBox1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Pixel(21D);
            this.htmlTextBox1.StyleName = "";
            this.htmlTextBox1.Value = "=TrimStart(Fields.InstallAddress)+\'<br />\'+Fields.InstallCity+\',\'+Fields.InstallS" +
    "tate+\' - \'+Fields.InstallPostCode";
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.3420963287353516D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.3617086410522461D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.1047978401184082D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.17077112197876D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.98662173748016357D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.2143750190734863D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox1);
            this.table1.Body.SetCellContent(0, 1, this.textBox3);
            this.table1.Body.SetCellContent(0, 3, this.textBox8);
            this.table1.Body.SetCellContent(0, 2, this.textBox10);
            this.table1.Body.SetCellContent(1, 0, this.textBox13);
            this.table1.Body.SetCellContent(1, 1, this.lblBalanceOwing, 1, 3);
            tableGroup17.Name = "tableGroup1";
            tableGroup17.ReportItem = this.textBox7;
            tableGroup18.Name = "tableGroup3";
            tableGroup18.ReportItem = this.textBox9;
            tableGroup19.Name = "group3";
            tableGroup19.ReportItem = this.textBox11;
            tableGroup20.Name = "tableGroup6";
            tableGroup20.ReportItem = this.textBox12;
            this.table1.ColumnGroups.Add(tableGroup17);
            this.table1.ColumnGroups.Add(tableGroup18);
            this.table1.ColumnGroups.Add(tableGroup19);
            this.table1.ColumnGroups.Add(tableGroup20);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1,
            this.textBox3,
            this.textBox10,
            this.textBox8,
            this.textBox13,
            this.lblBalanceOwing,
            this.textBox7,
            this.textBox9,
            this.textBox11,
            this.textBox12});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.95999997854232788D), Telerik.Reporting.Drawing.Unit.Cm(19.100000381469727D));
            this.table1.Name = "table1";
            tableGroup22.Name = "group4";
            tableGroup21.ChildGroups.Add(tableGroup22);
            tableGroup21.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup21.Name = "detailTableGroup1";
            tableGroup24.Name = "group2";
            tableGroup23.ChildGroups.Add(tableGroup24);
            tableGroup23.Name = "group1";
            this.table1.RowGroups.Add(tableGroup21);
            this.table1.RowGroups.Add(tableGroup23);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.979373931884766D), Telerik.Reporting.Drawing.Unit.Cm(3.7300140857696533D));
            this.table1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table1.Style.Font.Name = "Calibri";
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.3420963287353516D), Telerik.Reporting.Drawing.Unit.Cm(0.98662185668945312D));
            this.textBox1.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox1.Style.BorderColor.Default = System.Drawing.Color.Gray;
            this.textBox1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox1.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox1.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox1.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox1.Style.Font.Bold = false;
            this.textBox1.Style.Font.Name = "Calibri";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "=Fields.InvoicePayDate.ToShortDateString()";
            // 
            // textBox3
            // 
            this.textBox3.Format = "{0:0.00}";
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3617086410522461D), Telerik.Reporting.Drawing.Unit.Cm(0.98662197589874268D));
            this.textBox3.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox3.Style.BorderColor.Default = System.Drawing.Color.Gray;
            this.textBox3.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox3.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox3.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox3.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox3.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox3.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox3.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox3.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox3.Style.Font.Bold = false;
            this.textBox3.Style.Font.Name = "Calibri";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox3.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.Value = "=Fields.InvoicePayTotal";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.170771598815918D), Telerik.Reporting.Drawing.Unit.Cm(0.98662185668945312D));
            this.textBox8.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox8.Style.BorderColor.Default = System.Drawing.Color.Gray;
            this.textBox8.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox8.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox8.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox8.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox8.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox8.Style.Font.Bold = false;
            this.textBox8.Style.Font.Name = "Calibri";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox8.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.Value = "=Fields.InvoicePayMethod";
            // 
            // textBox10
            // 
            this.textBox10.Format = "{0:0.00}";
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1047978401184082D), Telerik.Reporting.Drawing.Unit.Cm(0.98662197589874268D));
            this.textBox10.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox10.Style.BorderColor.Default = System.Drawing.Color.Gray;
            this.textBox10.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox10.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox10.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox10.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox10.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox10.Style.Font.Bold = false;
            this.textBox10.Style.Font.Name = "Calibri";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox10.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox10.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.StyleName = "";
            this.textBox10.Value = "=Fields.CCSurcharge";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.3420963287353516D), Telerik.Reporting.Drawing.Unit.Cm(1.2143750190734863D));
            this.textBox13.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.textBox13.Style.BorderColor.Default = System.Drawing.Color.Gray;
            this.textBox13.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox13.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox13.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox13.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox13.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.textBox13.Style.Font.Name = "Calibri";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox13.StyleName = "";
            // 
            // lblBalanceOwing
            // 
            this.lblBalanceOwing.Name = "lblBalanceOwing";
            this.lblBalanceOwing.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.637277603149414D), Telerik.Reporting.Drawing.Unit.Cm(1.2143750190734863D));
            this.lblBalanceOwing.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.lblBalanceOwing.Style.BorderColor.Default = System.Drawing.Color.Gray;
            this.lblBalanceOwing.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.lblBalanceOwing.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.lblBalanceOwing.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.lblBalanceOwing.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.lblBalanceOwing.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Mm(0.10000000149011612D);
            this.lblBalanceOwing.Style.Color = System.Drawing.Color.White;
            this.lblBalanceOwing.Style.Font.Name = "Calibri";
            this.lblBalanceOwing.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.lblBalanceOwing.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(4D);
            this.lblBalanceOwing.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.lblBalanceOwing.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblBalanceOwing.StyleName = "";
            // 
            // table11
            // 
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.8529167175292969D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.87999999523162842D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.87999999523162842D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.87999999523162842D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.87999999523162842D)));
            this.table11.Body.SetCellContent(0, 0, this.txtfullretail);
            this.table11.Body.SetCellContent(1, 0, this.txtdiscount2);
            this.table11.Body.SetCellContent(2, 0, this.txtstc);
            this.table11.Body.SetCellContent(3, 0, this.txtgst);
            tableGroup25.Name = "tableGroup9";
            this.table11.ColumnGroups.Add(tableGroup25);
            this.table11.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtfullretail,
            this.txtdiscount2,
            this.txtstc,
            this.txtgst});
            this.table11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.500000953674316D), Telerik.Reporting.Drawing.Unit.Cm(12.59999942779541D));
            this.table11.Name = "table11";
            tableGroup27.Name = "group32";
            tableGroup28.Name = "group33";
            tableGroup29.Name = "group36";
            tableGroup30.Name = "group35";
            tableGroup26.ChildGroups.Add(tableGroup27);
            tableGroup26.ChildGroups.Add(tableGroup28);
            tableGroup26.ChildGroups.Add(tableGroup29);
            tableGroup26.ChildGroups.Add(tableGroup30);
            tableGroup26.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup26.Name = "detailTableGroup7";
            this.table11.RowGroups.Add(tableGroup26);
            this.table11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8529167175292969D), Telerik.Reporting.Drawing.Unit.Cm(3.5199999809265137D));
            this.table11.Style.Font.Name = "Calibri";
            // 
            // txtfullretail
            // 
            this.txtfullretail.Name = "txtfullretail";
            this.txtfullretail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8529167175292969D), Telerik.Reporting.Drawing.Unit.Cm(0.87999999523162842D));
            this.txtfullretail.Style.Color = System.Drawing.Color.White;
            this.txtfullretail.Style.Font.Name = "Calibri";
            this.txtfullretail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.txtfullretail.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtfullretail.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtfullretail.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtfullretail.Value = "=\"$ \"+Fields.lblfullretail";
            // 
            // txtdiscount2
            // 
            this.txtdiscount2.Name = "txtdiscount2";
            this.txtdiscount2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8529167175292969D), Telerik.Reporting.Drawing.Unit.Cm(0.87999999523162842D));
            this.txtdiscount2.Style.Color = System.Drawing.Color.White;
            this.txtdiscount2.Style.Font.Name = "Calibri";
            this.txtdiscount2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.txtdiscount2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtdiscount2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtdiscount2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtdiscount2.Value = "=\"$ \"+Fields.lbldiscount2";
            // 
            // txtstc
            // 
            this.txtstc.Name = "txtstc";
            this.txtstc.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8529167175292969D), Telerik.Reporting.Drawing.Unit.Cm(0.87999999523162842D));
            this.txtstc.Style.BorderColor.Default = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(55)))), ((int)(((byte)(94)))));
            this.txtstc.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.txtstc.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.txtstc.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.txtstc.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.txtstc.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.txtstc.Style.Color = System.Drawing.Color.White;
            this.txtstc.Style.Font.Name = "Calibri";
            this.txtstc.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.txtstc.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtstc.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtstc.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtstc.Value = "=\"$ \"+Fields.lblstc";
            // 
            // txtgst
            // 
            this.txtgst.Name = "txtgst";
            this.txtgst.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8529167175292969D), Telerik.Reporting.Drawing.Unit.Cm(0.87999999523162842D));
            this.txtgst.Style.Color = System.Drawing.Color.White;
            this.txtgst.Style.Font.Name = "Calibri";
            this.txtgst.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.txtgst.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtgst.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtgst.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtgst.Value = "=\"$ \"+Fields.lblgst";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(2.5999979972839355D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox2,
            this.textBox4,
            this.textBox5,
            this.textBox2,
            this.textBox14});
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(-0.10000196844339371D));
            this.pictureBox2.MimeType = "image/png";
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(2.7000000476837158D));
            this.pictureBox2.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.Normal;
            this.pictureBox2.Value = ((object)(resources.GetObject("pictureBox2.Value")));
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.5669293403625488D), Telerik.Reporting.Drawing.Unit.Inch(0.30936813354492188D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(0.699999988079071D));
            this.textBox4.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox4.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(24D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.Value = "www.arisesolar.com.au";
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.5669283866882324D), Telerik.Reporting.Drawing.Unit.Inch(0.629919707775116D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(0.699999988079071D));
            this.textBox5.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox5.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox5.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(24D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.Value = "ABN : 32168697907";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.59999942779541D), Telerik.Reporting.Drawing.Unit.Cm(-3.2297768939315574E-06D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9D), Telerik.Reporting.Drawing.Unit.Cm(0.699999988079071D));
            this.textBox2.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox2.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(24D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.Value = "payments@arisesolar.com.au";
            // 
            // textBox14
            // 
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045D), Telerik.Reporting.Drawing.Unit.Cm(0.499998539686203D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8000001907348633D), Telerik.Reporting.Drawing.Unit.Cm(1.5999991893768311D));
            this.textBox14.Style.Color = System.Drawing.Color.White;
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(42D);
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.Value = "1300 274 737";
            // 
            // new_paymentreceipt
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtdate,
            this.pageFooterSection1});
            this.Name = "Receipt";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Style.Font.Name = "open sp";
            this.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(21D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection txtdate;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.Table table8;
        private Telerik.Reporting.TextBox textBox126;
        private Telerik.Reporting.TextBox lblInvoiceSent;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox85;
        private Telerik.Reporting.TextBox textBox89;
        private Telerik.Reporting.TextBox textBox90;
        private Telerik.Reporting.TextBox textBox92;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox lblInvoiceNumber;
        private Telerik.Reporting.HtmlTextBox htmlTextBox1;
        private Telerik.Reporting.Table table11;
        private Telerik.Reporting.TextBox txtfullretail;
        private Telerik.Reporting.TextBox txtdiscount2;
        private Telerik.Reporting.TextBox txtstc;
        private Telerik.Reporting.TextBox txtgst;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox lblBalanceOwing;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.PictureBox pictureBox2;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox14;
    }
}