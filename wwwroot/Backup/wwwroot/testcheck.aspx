<%@ Page Language="C#" MasterPageFile="~/admin/templates/NoLayoutMasterPage.master" AutoEventWireup="true"
    CodeFile="testcheck.aspx.cs" Inherits="_testcheck" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div>
        <asp:Login ID="login1" runat="server" DestinationPageUrl="~/admin/adminfiles/dashboard.aspx" OnLoggedIn="Login1_LoggedIn" DisplayRememberMe="true" Width="100%">
            <LayoutTemplate>
                <div class="center-div">
                    <div class="login-form">
                        <div class="login-logo">
                            <img src="images/logo_Login.png">
                        </div>
                        <h2>Portal login</h2>
                        <div class="in">
                            <asp:TextBox ID="UserName" runat="server" placeholder="Username" class="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator
                                ID="UserNameRequired" runat="server" Display="dynamic" ControlToValidate="UserName" ErrorMessage=""
                                ValidationGroup="Login1"></asp:RequiredFieldValidator>
                        </div>
                        <div class="in">
                            <asp:TextBox ID="Password" runat="server" Display="dynamic" TextMode="Password" placeholder="Password" class="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator
                                ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage=""
                                ValidationGroup="Login1"></asp:RequiredFieldValidator>
                        </div>
                        <div class="loginbox-forgot">
                            <div class="checkbox">
                                <label>
                                    <asp:CheckBox ID="RememberMe" runat="server" />
                                    <span class="text">Remember me next time.</span>
                                </label>
                            </div>
                        </div>
                        <p class="center">
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/admin/forgotpassword.aspx">Forgot Password?</asp:HyperLink>
                        </p>
                        <div class="login-btn">
                            <asp:Button ID="LoginButton" runat="server" CommandName="Login" class="btn-col"
                                Text="Submit" ValidationGroup="Login1" />
                        </div>
                    </div>
                </div>
            </LayoutTemplate>
        </asp:Login>
    </div>
    <div class="text-center copytext">2017 Copyright <%=SiteName %> </div>
    <%-- <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=login1.FindControl("LoginButton").ClientID %>').click(function () {
                formValidate();
            });
        });
    </script>--%>
</asp:Content>