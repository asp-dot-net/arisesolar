﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="test.aspx.cs" Inherits="test" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <%--    <script src="<%=Siteurl %>admin/js/jquery-3.2.1.js"></script>--%>
        <%--  <script type="text/javascript" src="<%=Siteurl %>admin/js/jquery-1.8.2.min.js"></script>--%>
        <%--    <script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script>--%>
        <link href="http://portal.arisesolar.com.au/admin/css/Signature.css" rel="stylesheet" />
        <link href="http://portal.arisesolar.com.au/admin/css/jquery.signaturepad.css" rel="stylesheet" />
        <script src="http://portal.arisesolar.com.au/admin/js/jquery.signaturepad.min.js"></script>
        <script src="http://portal.arisesolar.com.au/admin/js/signature_pad.js"></script>
        <script src="http://portal.arisesolar.com.au/admin/js/signature_pad.min.js"></script>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script type="text/javascript"> 

        $(document).ready(function () {

            var latitude, longitude;

            $.get("http://ipinfo.io", function (response) {

                var location = response.loc;
                var index = location.indexOf(",");

                latitude = location.substring(0, index - 1); //lbllatitude
                longitude = location.substring(index + 1, index + 7);// lbllongitude                
                        $("#<%=lbllatitude.ClientID %>").html(latitude)
                        $("#<%=lbllongitude.ClientID %>").html(longitude)

            }, "jsonp");
        });


    </script>

    <script>
        function validate() {
            <%--var ext = file.split(".");
            ext = ext[ext.length - 1].toLowerCase();
            var arrayExtensions = ["jpg", "jpeg", "png", "bmp", "gif"];

            if (arrayExtensions.lastIndexOf(ext) == -1) {
                alert("Wrong extension type.");
                $("#<%=fuDocument.ClientID%>").val("");
            }--%>

            var extension = $("#<%=fuDocument.ClientID%>").val().split('.').pop().toLowerCase();
            var Test = $("#<%=Test.ClientID%>").val();

            if (Test == "Nearmap") {
                if ($.inArray(extension, ['pdf', 'PDF']) == -1) {
                    alert('Sorry, invalid extension.');
                    $("#<%=fuDocument.ClientID%>").val("");
                    return false;
                }
            }

        }

        function validate1() {
            var extension = $("#<%=fuDocument.ClientID%>").val().split('.').pop().toLowerCase();
            var ddlDocument = $("#<%=Test.ClientID%>").val();
            var errorMsg = $("#errorMsg");

            if (ddlDocument == "Nearmap") {
                if ($.inArray(extension, ['pdf', 'PDF']) == -1) {
                    errorMsg.html("pdf Only..")
                    $("#<%=fuDocument.ClientID%>").val("");
                    return false;
                }
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:DropDownList runat="server" ID="Test" onChange="validate()">
                <asp:ListItem Text="Nearmap" Value="Nearmap" />
                <asp:ListItem Text="Test" Value="Test" />
            </asp:DropDownList>
            <asp:FileUpload ID="fuDocument" runat="server" onChange="validate1()" />
            <div id="errorMsg" style="color: red"></div>
            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidatorFuDocument" runat="server" ControlToValidate="fuDocument"
                ValidationGroup="DocUpload" ValidationExpression="^.+(.pdf|.PDF)$" ForeColor="Red"
                Display="Dynamic" ErrorMessage=".pdf only" class="error_text"></asp:RegularExpressionValidator>--%>
        </div>

        <div>
            <table align="center" border="0" cellpadding="0" cellspacing="0" style="padding: 10px 20px; max-width: 730px;">
                <tbody>
                    <tr>
                        <td style="line-height: 24px; font-size: 14px; font-family: arial; margin: 0px; padding: 5px 0px">Customer name:
                                                <asp:Label runat="server" ID="lblCustName2"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 24px; font-size: 14px; font-family: arial; margin: 0px; padding: 5px 0px">Date:
                                                <asp:Label runat="server" ID="lblTodayDate"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 24px; font-size: 14px; font-family: arial; margin: 0px; padding: 5px 0px">Email:
                                                <asp:Label runat="server" ID="lblCustEmailID"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 24px; font-size: 14px; font-family: arial; margin: 0px; padding: 5px 0px">Captured IP Address:
                                                <asp:Label runat="server" ID="lblIPAdd"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 24px; font-size: 14px; font-family: arial; margin: 0px; padding: 5px 0px">Location:
                                                <asp:Label runat="server" ID="lblDeviceLocation"></asp:Label>
                            Latitude:<asp:Label runat="server" ID="lbllatitude"></asp:Label><br />
                            <div style="padding-left: 60px;">Longitide:<asp:Label runat="server" ID="lbllongitude" /></label></div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
