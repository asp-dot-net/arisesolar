﻿using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Text;
using System.Web.UI;
using System.ComponentModel.DataAnnotations;

public partial class GreenBot_Default : System.Web.UI.Page
{
    static string AccessToken = "";
    protected void Page_Load(object sender, EventArgs e)
    {
       
       
    }

     async Task RunAsync()
    {
        HttpClient client = new HttpClient();
        client.DefaultRequestHeaders.Accept.Clear();
        client.DefaultRequestHeaders.Accept.Add(
        new MediaTypeWithQualityHeaderValue("application/json"));

        try
        {
            JObject oJsonObject = new JObject();
            oJsonObject.Add("Username", "devapi");
            oJsonObject.Add("Password", "devapi");
            var oTaskPostAsync = await client.PostAsync("https://api.greenbot.com.au/api/Account/Login", new StringContent(oJsonObject.ToString(), Encoding.UTF8, "application/json"));
            bool IsSuccess = oTaskPostAsync.IsSuccessStatusCode;
            var tokenJson = await oTaskPostAsync.Content.ReadAsStringAsync();
            RootObject RootObject = JsonConvert.DeserializeObject<RootObject>(tokenJson);
            AccessToken = RootObject.TokenData.access_token;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }

     async Task CreateJobAsync()
    {
        HttpClient client = new HttpClient();
        client.DefaultRequestHeaders.Accept.Clear();
        client.DefaultRequestHeaders.Accept.Add(
        new MediaTypeWithQualityHeaderValue("application/json"));
        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AccessToken);
        try
        {
                var content = new CreateJob {
                    VendorJobId = 1146,
                    BasicDetails = new BasicDetails
                    {
                        JobType = 1,
                        RefNumber = "VendorApi",
                        JobStage = 1,
                        Title = "New",
                        StrInstallationDate =  Convert.ToDateTime("2018/02/02"),
                        Priority = 1,
                        Description = "Abc_VendorApi"
                    },
                    JobOwnerDetails = new JobOwnerDetails
                    {
                        OwnerType = "Individual",
                        //CompanyName = "Customer Name",
                        FirstName = "Richa",
                        LastName = "Gajjar",
                        Email = "richa@meghtechnologies.com",
                        Phone = "65981245",
                        Mobile = "951785612",
                        AddressID = 1,
                       // UnitTypeID = "1",
                       // UnitNumber = "65",
                        StreetName = "Street1",
                        StreetNumber = "15",
                        StreetTypeId = 1,
                        Town = "MELROSE",
                        State = "QLD",
                        PostCode = "4613",
                        IsPostalAddress = false
                    },
                    JobInstallationDetails = new JobInstallationDetails
                    {
                        AddressId = 1,
                        //UnitTypeID = 1,
                        //UnitNumber = "123",
                        StreetName = "Street2",
                        StreetNumber = "45",
                        StreetTypeId = 1,
                        Town = "MELROSE",
                        State = "QLD",
                        PostCode = "4613",
                        //isPostalAddress = false,
                        //DistributorID = 1,
                        //ElectricityProviderID = 1,
                        //NMI = "NMI",
                        //PhaseProperty = "PhaseProp",
                        NoOfPanels = 2,
                        ExistingSystem = true
                    },
                    JobSystemDetails = new JobSystemDetails
                    {
                        SystemSize = 45
                        //InstallationType = "Other"
                        //SystemModel = "TestModel",
                        //NoOfPanel = 5
                    },
                    JobStcDetails = new JobStcDetails
                    {
                        TypeOfConnection = "Connection Type",
                        SystemMountingType = "Type 1",
                        DeemingPeriod = "13",
                        MultipleSguAddress = "No"
                    },
                    InstallerView = new InstallerView
                    {
                        CECAccreditationNumber = "A0159429",
                        SeDesignRoleId = 1,
                        //Email = "richa@meghtechnologies.com",
                        FirstName = "Robert",
                        LastName = "Street",
                        Phone = "444444",
                        Mobile = "978564123",
                        //ElectricalContractorsLicenseNumber = "9876543214",
                        AddressID = 1,
                        //UnitTypeID = 1,
                        //UnitNumber = "1",
                        StreetName = "Wales",
                        StreetNumber ="123",
                        StreetTypeId = 1,
                        State = "QLD",
                        Town = "MELROSE",
                        PostCode = "4613",
                        IsPostalAddress = true,
                    }

            };
            var temp = JsonConvert.SerializeObject(content);
            var oTaskPostAsync = await client.PostAsync("https://api.greenbot.com.au/api/Account/CreateJob", new StringContent(temp.ToString(), Encoding.UTF8, "application/json"));
            bool IsSuccess = oTaskPostAsync.IsSuccessStatusCode;
            
            var JsonString = await oTaskPostAsync.Content.ReadAsStringAsync();
            RootObj RootObject = JsonConvert.DeserializeObject<RootObj>(JsonString);
            Response.Write(JsonString + "   "+IsSuccess.ToString() +" Message: "+RootObject.Message +" statusCode: "+RootObject.StatusCode +"Status:"+RootObject.Status);
            Response.End();
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }

    protected async void btn1_Click(object sender, EventArgs e)
    {
        await RunAsync();
    }

    protected async void CreateJob_Click(object sender, EventArgs e)
    {
        await CreateJobAsync();
    }

    public class TokenData
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }

    public class RootObject
    {
        public TokenData TokenData { get; set; }
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
    }

    public class RootObj
    {
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
    }

    #region CreateJob Model
    public partial class CreateJob
    {
        [JsonProperty("VendorJobId")]
        public int VendorJobId { get; set; }

        [JsonProperty("BasicDetails")]
        public BasicDetails BasicDetails { get; set; }

        [JsonProperty("InstallerView")]
        public InstallerView InstallerView { get; set; }

        [JsonProperty("JobInstallationDetails")]
        public JobInstallationDetails JobInstallationDetails { get; set; }

        [JsonProperty("JobOwnerDetails")]
        public JobOwnerDetails JobOwnerDetails { get; set; }

        [JsonProperty("JobSystemDetails")]
        public JobSystemDetails JobSystemDetails { get; set; }

        [JsonProperty("JobSTCDetails")]
        public JobStcDetails JobStcDetails { get; set; }

        [JsonProperty("panel")]
        public Panel[] Panel { get; set; }

        [JsonProperty("inverter")]
        public Inverter[] Inverter { get; set; }

        [JsonProperty("lstJobNotes")]
        public LstJobNote[] LstJobNotes { get; set; }

        [JsonProperty("lstCustomDetails")]
        public LstCustomDetail[] LstCustomDetails { get; set; }
    }

    public partial class BasicDetails
    {
        [JsonProperty("JobType")]
        public int JobType { get; set; }

        [JsonProperty("RefNumber")]
        public string RefNumber { get; set; }

        [JsonProperty("JobStage")]
        public int JobStage { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("strInstallationDate")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime StrInstallationDate { get; set; }

        [JsonProperty("Priority")]
        public int Priority { get; set; }

        //[JsonProperty("IsClassic")]
        //public bool IsClassic { get; set; }

        //[JsonProperty("IsGst")]
        //public bool IsGst { get; set; }

        //[JsonProperty("GSTDocument")]
        //public string GstDocument { get; set; }

        //[JsonProperty("GstFileBase64")]
        //public string GstFileBase64 { get; set; }
    }

    public partial class InstallerView
    {
        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("UnitTypeID")]
        public int UnitTypeID { get; set; }

        [JsonProperty("PostCode")]
        public string PostCode { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("StreetName")]
        public string StreetName { get; set; }

        [JsonProperty("StreetNumber")]
        public string StreetNumber { get; set; }

        [JsonProperty("StreetTypeID")]
        public int StreetTypeId { get; set; }

        [JsonProperty("AddressID")]
        public int AddressID { get; set; }

        [JsonProperty("Town")]
        public string Town { get; set; }

        [JsonProperty("CECAccreditationNumber")]
        public string CECAccreditationNumber { get; set; }

        [JsonProperty("SEDesignRoleId")]
        public int SeDesignRoleId { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("Mobile")]
        public string Mobile { get; set; }

        [JsonProperty("ElectricalContractorsLicenseNumber")]
        public string ElectricalContractorsLicenseNumber { get; set; }
        
        [JsonProperty("UnitNumber")]
        public string UnitNumber { get; set; }

        [JsonProperty("IsPostalAddress")]
        public bool IsPostalAddress { get; set; }
        //[JsonProperty("SignBase64")]
        //public string SignBase64 { get; set; }

        //[JsonProperty("SESignature")]
        //public string SeSignature { get; set; }

        //[JsonProperty("Latitude")]
        //public string Latitude { get; set; }

        //[JsonProperty("Longitude")]
        //public string Longitude { get; set; }
    }

    public partial class Inverter
    {
        [JsonProperty("Brand")]
        public string Brand { get; set; }

        [JsonProperty("Series")]
        public string Series { get; set; }

        [JsonProperty("Model")]
        public string Model { get; set; }
    }

    public partial class JobInstallationDetails
    {
        [JsonProperty("UnitTypeID")]
        public int UnitTypeID { get; set; }

        [JsonProperty("UnitNumber")]
        public string UnitNumber { get; set; }

        [JsonProperty("PostCode")]
        public string PostCode { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("StreetName")]
        public string StreetName { get; set; }

        [JsonProperty("StreetNumber")]
        public string StreetNumber { get; set; }

        [JsonProperty("StreetTypeID")]
        public int StreetTypeId { get; set; }

        [JsonProperty("Town")]
        public string Town { get; set; }

        [JsonProperty("AddressID")]
        public int AddressId { get; set; }

        [JsonProperty("PostalAddressID")]
        public int PostalAddressId { get; set; }

        [JsonProperty("PropertyType")]
        public string PropertyType { get; set; }

        [JsonProperty("SingleMultipleStory")]
        public string SingleMultipleStory { get; set; }

        [JsonProperty("InstallingNewPanel")]
        public string InstallingNewPanel { get; set; }

        [JsonProperty("ExistingSystem")]
        public bool ExistingSystem { get; set; }

        [JsonProperty("ExistingSystemSize")]
        public string ExistingSystemSize { get; set; }

        [JsonProperty("NoOfPanels")]
        public int NoOfPanels { get; set; }

        [JsonProperty("SystemLocation")]
        public string SystemLocation { get; set; }

        [JsonProperty("AdditionalInstallationInformation")]
        public string AdditionalInstallationInformation { get; set; }

        [JsonProperty("DistributorID")]
        public int DistributorID { get; set; }
        
        [JsonProperty("ElectricityProviderID")]
        public int ElectricityProviderID { get; set; }

        [JsonProperty("NMI")]
        public string NMI { get; set; }

        [JsonProperty("MeterNumber")]
        public string MeterNumber { get; set; }

        [JsonProperty("PhaseProperty")]
        public string PhaseProperty { get; set; }

    }

    public partial class JobOwnerDetails
    {
        [JsonProperty("OwnerType")]
        public string OwnerType { get; set; }

        [JsonProperty("CompanyName")]
        public string CompanyName { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("Phone")]
        public string Phone { get; set; }

        [JsonProperty("Mobile")]
        public string Mobile { get; set; }

        [JsonProperty("PostCode")]
        public string PostCode { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("UnitTypeID")]
        public string UnitTypeID { get; set; }

        [JsonProperty("StreetName")]
        public string StreetName { get; set; }

        [JsonProperty("StreetNumber")]
        public string StreetNumber { get; set; }

        [JsonProperty("StreetTypeID")]
        public int StreetTypeId { get; set; }

        [JsonProperty("Town")]
        public string Town { get; set; }

        [JsonProperty("AddressID")]
        public int AddressID { get; set; }

        [JsonProperty("IsPostalAddress")]
        public bool IsPostalAddress { get; set; }

        [JsonProperty("UnitNumber")]
        public string UnitNumber { get; set; }
    }

    public partial class JobStcDetails
    {
        [JsonProperty("TypeOfConnection")]
        public string TypeOfConnection { get; set; }

        [JsonProperty("SystemMountingType")]
        public string SystemMountingType { get; set; }

        [JsonProperty("DeemingPeriod")]
        public string DeemingPeriod { get; set; }

        [JsonProperty("MultipleSGUAddress")]
        public string MultipleSguAddress { get; set; }

        [JsonProperty("Location")]
        public string Location { get; set; }
    }

    public partial class JobSystemDetails
    {
        [JsonProperty("SystemModel")]
        public string SystemModel { get; set; }

        [JsonProperty("SystemSize")]
        public decimal SystemSize { get; set; }

        [JsonProperty("SerialNumbers")]
        public string SerialNumbers { get; set; }

        [JsonProperty("NoOfPanel")]
        public int NoOfPanel { get; set; }

        [JsonProperty("InstallationType")]
        public string InstallationType { get; set; }

    }

    public partial class LstCustomDetail
    {
        [JsonProperty("VendorJobCustomFieldId")]
        public long VendorJobCustomFieldId { get; set; }

        [JsonProperty("FieldValue")]
        public string FieldValue { get; set; }

        [JsonProperty("FieldName")]
        public string FieldName { get; set; }
    }

    public partial class LstJobNote
    {
        [JsonProperty("VendorJobNoteId")]
        public string VendorJobNoteId { get; set; }

        [JsonProperty("Notes")]
        public string Notes { get; set; }
    }

    public partial class Panel
    {
        [JsonProperty("Brand")]
        public string Brand { get; set; }

        [JsonProperty("Model")]
        public string Model { get; set; }

        [JsonProperty("NoOfPanel")]
        public string NoOfPanel { get; set; }
    }
    #endregion
}